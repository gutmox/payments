# Payments API

[![pipeline status](https://gitlab.com/gutmox/payments/badges/master/pipeline.svg)](https://gitlab.com/gutmox/payments/commits/master)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=coverage)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=bugs)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)

Payments RESTFUL API

Features:

* Fetches a single payment resource
* Creates a payment resource
* List a collection of payment resources
* Persists resource state in MongoDb


## Swagger API Definition

[![Swagger definition](https://gitlab.com/gutmox/payments/raw/master/src/main/resources/swagger.png)](https://app.swaggerhub.com/apis-docs/gutmox/Payments/1.0)
(click on image to see the API design)

## Create Payment Sequence

```mermaid
sequenceDiagram
    participant App
    participant Vertx_Routing
    participant CreatePaymentsHandler
    participant PaymentsApiImpl
    App->> Vertx_Routing: POST /transaction/payments
    Vertx_Routing->> CreatePaymentsHandler: handle
    CreatePaymentsHandler->> PaymentsApiImpl: createPayment
    PaymentsApiImpl ->> PaymentsValidations: isValid(payment)
    PaymentsApiImpl ->> PaymentsRepository: save(payment)
    PaymentsRepository ->> MongoClient: rxSave(collection, payment)
```

## Sonar Report

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.gutmox.payments%3Apayments&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)

[sonarcloud report](https://sonarcloud.io/dashboard?id=com.gutmox.payments%3Apayments)


## Built With

* [Java 8](http://www.oracle.com/technetwork/java/javase/10-relnote-issues-4108729.html) - Code language 
* [Vert.x](https://vertx.io) - REST library used
* [RX Java 2](https://github.com/ReactiveX/RxJava) - Reactive Extensions for the JVM library
* [Gradle](https://gradle.org) - Build tool
* [Guice](https://github.com/google/guice) - Dependency injection

### Unit Testing

* [JUnit 5](https://junit.org/junit5/) - Junit
* [Mockito](http://site.mockito.org)   - Mocking 
* [AssertJ](http://joel-costigliola.github.io/assertj/index.html) - adds fluent assertion support

## Running It

## From command line

```
    ./gradlew clean run
```

## From your IDE

Configure to Launch:

![IntelliJ Config](https://gitlab.com/gutmox/payments/raw/master/src/main/resources/intellij-run.png)

## Running tests

### Unit tests

```
    ./gradlew clean test
```

### Acceptance tests

```
    ./gradlew clean execMongo test stopMongo -Dtest.profile=acceptance
```

## Building
```
    ./gradlew clean build
```

## To package

```
   ./gradlew clean assemble
```


## Docker

### To build:

```
    docker build -t payments .
 ```
### To run:

```
    docker run -t -i -p 8080:8080 payments
```