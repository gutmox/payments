###
#
# To build:
#
#  docker build -t payments .
#
# To run:
#
#  docker run -t -i -p 8080:8080 payments
#
###

FROM java:8-jre

ENV DIST_FILE build/distributions/payments-1.0-SNAPSHOT.tar

# Set the location of the verticles
ENV VERTICLE_HOME /opt/verticles

EXPOSE 8080

COPY $DIST_FILE $VERTICLE_HOME/

WORKDIR $VERTICLE_HOME

RUN tar -xvf payments-1.0-SNAPSHOT.tar

ENTRYPOINT ["sh", "-c"]
CMD ["./payments-1.0-SNAPSHOT/bin/payments run com.gutmox.payments.MainVerticle"]
