package com.gutmox.payments.routing;

import com.google.inject.Inject;
import com.gutmox.payments.handlers.CreatePaymentsHandler;
import com.gutmox.payments.handlers.GetAllPaymentsHandler;
import com.gutmox.payments.handlers.GetPaymentHandler;
import com.gutmox.payments.handlers.StatusHandler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.reactivex.ext.web.handler.ResponseTimeHandler;

public class Routing {

    private static final Logger LOGGER = LoggerFactory.getLogger(Routing.class);

    private final StatusHandler statusHandler;

    private final CreatePaymentsHandler createPaymentsHandler;

    private final GetPaymentHandler getPaymentHandler;

    private final GetAllPaymentsHandler getAllPaymentsHandler;

    private static final String PAYMENTS_URL = "/transaction/payments";

    private Router router;


    @Inject
    public Routing(StatusHandler statusHandler,
                   CreatePaymentsHandler createPaymentsHandler,
                   GetPaymentHandler getPaymentHandler,
                   GetAllPaymentsHandler getAllPaymentsHandler) {
        this.statusHandler = statusHandler;
        this.createPaymentsHandler = createPaymentsHandler;
        this.getPaymentHandler = getPaymentHandler;
        this.getAllPaymentsHandler = getAllPaymentsHandler;
    }

    public Router getRouter() {

        long bodyLimit = 1024;

        router.route().handler(ResponseTimeHandler.create());

        router.get("/status").handler(statusHandler);

        router.post(PAYMENTS_URL).handler(BodyHandler.create().setBodyLimit(bodyLimit * bodyLimit));
        router.post(PAYMENTS_URL).handler(createPaymentsHandler);
        router.get(PAYMENTS_URL).handler(getAllPaymentsHandler);
        router.get(PAYMENTS_URL + "/:paymentIdentifier").handler(getPaymentHandler);

        LOGGER.debug("Routing Done");

        return router;
    }


    public void setRouter(Router router) {
        this.router = router;
    }
}
