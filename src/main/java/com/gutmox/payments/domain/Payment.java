package com.gutmox.payments.domain;

import com.google.common.base.Objects;
import io.vertx.core.json.JsonObject;


public class Payment {

    private JsonObject bodyAsJson;

    public static Builder builder() {
        return new Builder();
    }


    public static final class Builder {

        private JsonObject bodyAsJson;

        private Builder() {
            bodyAsJson = new JsonObject();
        }

        public Builder withId(String id) {
            bodyAsJson.put("id", id);
            return this;
        }


        public Payment build() {
            Payment payment = new Payment();
            payment.bodyAsJson = this.bodyAsJson;
            return payment;
        }

        public Builder withBodyAsJson(JsonObject bodyAsJson) {
            this.bodyAsJson = bodyAsJson;
            return this;
        }

    }

    public String getId() {
        return bodyAsJson.getString("id");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return Objects.equal(bodyAsJson, payment.bodyAsJson);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(bodyAsJson);
    }

    public JsonObject getBodyAsJson() {
        return bodyAsJson;
    }

}

