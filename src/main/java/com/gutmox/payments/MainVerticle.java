package com.gutmox.payments;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.gutmox.payments.guice.GuiceModule;
import com.gutmox.payments.mongo.config.MongoConfiguration;
import com.gutmox.payments.routing.Routing;
import io.reactivex.Observable;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.http.HttpServer;
import io.vertx.reactivex.ext.web.Router;

import java.util.logging.Logger;

public class MainVerticle extends AbstractVerticle {

    private final Logger logger = Logger.getLogger("MainVerticle");

    private static Integer port = 8080;

    @Inject
    private Routing routing;

    @Override
    public void start(Future<Void> startFuture) throws Exception {

        startServer().subscribe(
                t -> {
                    startFuture.complete();
                    logger.info(MainVerticle.class.getName() + " Running on " + port + " !!!!!!! ");
                });
    }

    private Observable<HttpServer> startServer() {
        MongoConfiguration.setVertx(vertx);
        Guice.createInjector(new GuiceModule(vertx)).injectMembers(this);
        HttpServerOptions options = new HttpServerOptions().setCompressionSupported(true);
        HttpServer httpServer = vertx.createHttpServer(options);

        routing.setRouter(Router.router(vertx));
        return httpServer.requestHandler(routing.getRouter()).rxListen(port).toObservable();
    }
}
