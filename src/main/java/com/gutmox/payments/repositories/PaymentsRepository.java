package com.gutmox.payments.repositories;

import com.gutmox.payments.mongo.config.MongoConfiguration;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.mongo.MongoClient;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

public class PaymentsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentsRepository.class);

    private MongoClient mongoClient;

    private String collection = "payments";

    @Inject
    public PaymentsRepository() {

        this(MongoConfiguration.init());
    }

    public PaymentsRepository(MongoClient mongoClient) {

        this.mongoClient = mongoClient;
    }

    public Single<String> save(JsonObject account) {

        String paymentIdentifier = UUID.randomUUID().toString();

        LOGGER.info("paymentIdentifier : " + paymentIdentifier);

        account.put("_id", paymentIdentifier);

        mongoClient.rxSave(collection, account).subscribe();

        return Single.just(paymentIdentifier);
    }

    public Single<JsonObject> get(String paymentIdentifier) {

        LOGGER.info("paymentIdentifier : " + paymentIdentifier);

        return mongoClient.rxFindOne(collection, new JsonObject().put("_id", paymentIdentifier), null);
    }

    public Single<List<JsonObject>> getAll() {

        LOGGER.info("getAll ");

        return mongoClient.rxFind(collection, new JsonObject());
    }
}
