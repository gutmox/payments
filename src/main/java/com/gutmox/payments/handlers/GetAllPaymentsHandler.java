package com.gutmox.payments.handlers;

import com.google.inject.Inject;
import com.gutmox.payments.api.PaymentsApi;
import com.gutmox.payments.api.PaymentsApiImpl;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.RoutingContext;

import java.util.List;

public class GetAllPaymentsHandler implements Handler<RoutingContext> {

    private final PaymentsApi paymentsApi;

    @Inject
    GetAllPaymentsHandler(PaymentsApiImpl paymentsApi) {
        this.paymentsApi = paymentsApi;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(GetAllPaymentsHandler.class);

    @Override
    public void handle(RoutingContext context) {

        String paymentIdentifier = context.request().getParam("paymentIdentifier");
        LOGGER.info("paymentIdentifier: " + paymentIdentifier);

        paymentsApi.getAllPayments().subscribe(payments -> {

            LOGGER.info("Result : " + payments);


            context.response().putHeader("content-type", "application/json")
                    .end((tojsonList(payments)));
        });
    }

    private String tojsonList(List<JsonObject> payments) {
        JsonArray js = new JsonArray();
        payments.forEach(js::add);
        return js.encode();
    }
}
