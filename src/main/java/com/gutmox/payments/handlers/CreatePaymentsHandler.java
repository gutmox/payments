package com.gutmox.payments.handlers;

import com.gutmox.payments.api.PaymentsApi;
import com.gutmox.payments.api.PaymentsApiImpl;
import com.gutmox.payments.domain.Payment;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.RoutingContext;

import javax.inject.Inject;

public class CreatePaymentsHandler implements Handler<RoutingContext> {

    private final PaymentsApi paymentsApi;

    @Inject
    public CreatePaymentsHandler(PaymentsApiImpl paymentsApi) {
        this.paymentsApi = paymentsApi;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePaymentsHandler.class);

    @Override
    public void handle(RoutingContext context) {

        LOGGER.info("Body : " + context.getBodyAsString());

        paymentsApi.createPayment(Payment.builder()
                .withBodyAsJson(context.getBodyAsJson()).build())
                .subscribe(paymentIdentifier -> {

            LOGGER.info("Result : " + paymentIdentifier);

            context.response().putHeader("content-type", "application/json")
                    .end(context.getBodyAsJson()
                            .put("payment_identifier", paymentIdentifier)
                            .encode());
        });
    }
}
