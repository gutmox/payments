package com.gutmox.payments.handlers;

import com.google.inject.Inject;
import com.gutmox.payments.api.PaymentsApi;
import com.gutmox.payments.api.PaymentsApiImpl;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.ext.web.RoutingContext;

public class GetPaymentHandler implements Handler<RoutingContext> {

    private final PaymentsApi paymentsApi;

    @Inject
    GetPaymentHandler(PaymentsApiImpl paymentsApi) {
        this.paymentsApi = paymentsApi;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(GetPaymentHandler.class);

    @Override
    public void handle(RoutingContext context) {

        String paymentIdentifier = context.request().getParam("paymentIdentifier");
        LOGGER.info("paymentIdentifier: " + paymentIdentifier);

        paymentsApi.getPayment(paymentIdentifier).subscribe(payment -> {

            LOGGER.info("Result : " + payment.getBodyAsJson());

            if (payment.getBodyAsJson() == null){
                context.response().putHeader("content-type", "application/json")
                        .end();
            } else {
                context.response().putHeader("content-type", "application/json")
                        .end((payment.getBodyAsJson().encode()));
            }
        });
    }
}
