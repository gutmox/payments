package com.gutmox.payments.mongo.config;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.mongo.MongoClient;

public class MongoConfiguration {

    private MongoConfiguration() {
    }

    private static Vertx vertx;

    public static MongoClient init() {
        if (vertx == null){
            return null;
        }
        return MongoClient.createNonShared(vertx, config());
    }

    private static JsonObject config() {

        JsonObject config = new JsonObject();

        config.put("connection_string", "mongodb://localhost:27017");

        config.put("db_name", "payments");

        return config;
    }

    public static void setVertx(Vertx vertx) {
        MongoConfiguration.vertx = vertx;
    }
}