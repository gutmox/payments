package com.gutmox.payments.api;

import com.google.inject.Inject;
import com.gutmox.payments.domain.Payment;
import com.gutmox.payments.exceptions.InvalidPayment;
import com.gutmox.payments.repositories.PaymentsRepository;
import com.gutmox.payments.validations.PaymentsValidations;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;

import java.util.List;

public class PaymentsApiImpl implements PaymentsApi {


    private final PaymentsRepository paymentsRepository;

    private final PaymentsValidations paymentsValidations;

    @Inject
    PaymentsApiImpl(PaymentsRepository paymentsRepository, PaymentsValidations paymentsValidations) {
        this.paymentsRepository = paymentsRepository;
        this.paymentsValidations = paymentsValidations;
    }

    @Override
    public Single<String> createPayment(Payment payment) {

        return paymentsValidations.isValid(payment).flatMap(isValid -> {
            if (!isValid) {
                throw new InvalidPayment();
            }
            return paymentsRepository.save(payment.getBodyAsJson());
        });
    }

    @Override
    public Single<Payment> getPayment(String paymentIdentifier) {
        return paymentsRepository.get(paymentIdentifier).map(json -> Payment.builder().withBodyAsJson(json).build());
    }

    @Override
    public Single<List<JsonObject>> getAllPayments() {
        return paymentsRepository.getAll();
    }
}
