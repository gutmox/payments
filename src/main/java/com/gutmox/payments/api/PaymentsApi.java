package com.gutmox.payments.api;


import com.gutmox.payments.domain.Payment;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;

import java.util.List;

public interface PaymentsApi {

    Single<String> createPayment(Payment payment);

    Single<Payment> getPayment(String paymentIdentifier);

    Single<List<JsonObject>> getAllPayments();
}
