package com.gutmox.payments.validations;

import com.gutmox.payments.domain.Payment;
import io.reactivex.Single;

public class PaymentsValidations {

    public Single<Boolean> isValid(Payment payment) {

        return Single.just(! (payment.getId() != null && payment.getId().isEmpty()));
    }
}
