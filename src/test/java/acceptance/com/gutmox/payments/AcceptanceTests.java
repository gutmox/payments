package acceptance.com.gutmox.payments;

import com.gutmox.payments.MainVerticle;
import com.gutmox.payments.domain.Payment;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.ext.web.client.WebClient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(VertxExtension.class)
class AcceptanceTests {

    private static final int PORT = 8080;

    private static WebClient client;

    @BeforeAll
    static void deployVerticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(new MainVerticle(), testContext.succeeding(id -> testContext.completeNow()));
        client = WebClient.create(vertx);
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void status_should_return_ok(Vertx vertx, VertxTestContext testContext) {

        client.get(PORT, "localhost", "/status").send(response -> {
            testContext.verify(() -> {
                assertTrue(response.succeeded());
                assertThat(response.result().statusCode()).isEqualTo(200);
                assertThat(response.result().bodyAsString()).isEqualTo("{\"status\":\"Show must go on\"}");
                testContext.completeNow();
            });
        });
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void create_payments_should_return_Ok(Vertx vertx, VertxTestContext testContext) {
        client.post(PORT, "localhost", "/transaction/payments")
                .sendJson(Payment.builder().withId(UUID.randomUUID().toString()).build(), response -> {
                    assertTrue(response.succeeded());
                    assertThat(response.result().statusCode()).isEqualTo(200);
                    testContext.completeNow();
                });
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void get_single_payment_should_return_Ok(Vertx vertx, VertxTestContext testContext) {

        client.get(PORT, "localhost", "/transaction/payments/5287ae0b-aafb-4087-8de7-2286892e7132").send(response -> {
            testContext.verify(() -> {
                assertTrue(response.succeeded());
                testContext.completeNow();
            });
        });
    }

    @Test
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void get_should_return_previous_stored_payment(Vertx vertx, VertxTestContext testContext) {

        client.post(PORT, "localhost", "/transaction/payments")
                .sendJson(Payment.builder().withId(UUID.randomUUID().toString()).build(), postResponse -> {
                    testContext.verify(() -> {
                        String id = Payment.builder().withBodyAsJson(postResponse.result().bodyAsJsonObject()).build().getId();

                        client.get(PORT, "localhost", "/transaction/payments/" + id)
                                .send(response -> {
                                    assertTrue(response.succeeded());
                                    assertThat(response.result().statusCode()).isEqualTo(200);
                                    testContext.completeNow();
                                });
                    });
                });
    }
}
