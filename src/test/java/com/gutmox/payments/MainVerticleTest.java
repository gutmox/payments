package com.gutmox.payments;

import io.vertx.core.Vertx;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Java6Assertions.assertThat;

@ExtendWith(VertxExtension.class)
class MainVerticleTest {

    private static final int PORT = 8080;

    @BeforeEach
    void deploy_verticle(Vertx vertx, VertxTestContext testContext) {
        vertx.deployVerticle(new MainVerticle(), testContext.succeeding(id -> testContext.completeNow()));
    }

    @Test
    @DisplayName("Should start a Web Server on port 8080")
    @Timeout(value = 10, timeUnit = TimeUnit.SECONDS)
    void start_http_server(Vertx vertx, VertxTestContext testContext) throws Throwable {
        vertx.createHttpClient().getNow(PORT, "localhost", "/status", response -> testContext.verify(() -> {
            assertThat(response.statusCode()).isEqualTo(200);
            response.handler(body -> {
                assertThat(body.toString()).isEqualTo("{\"status\":\"Show must go on\"}");
                testContext.completeNow();
            });
        }));
    }
}
