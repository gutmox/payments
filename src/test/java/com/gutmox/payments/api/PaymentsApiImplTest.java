package com.gutmox.payments.api;

import com.gutmox.payments.domain.Payment;
import com.gutmox.payments.repositories.PaymentsRepository;
import com.gutmox.payments.validations.PaymentsValidations;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class PaymentsApiImplTest {

    private PaymentsApi paymentsApi;

    @Mock
    private PaymentsRepository paymentsRepository;

    @Mock
    private PaymentsValidations paymentsValidations;

    private JsonObject jsonObject;

    private Payment payment;

    private String paymentIdentifier = UUID.randomUUID().toString();

    private List<JsonObject> payments;

    @BeforeEach
    void setUp() {

        paymentsApi = new PaymentsApiImpl(paymentsRepository, paymentsValidations);

        jsonObject = new JsonObject()
                .put("payment_identifier", paymentIdentifier);

        payment = Payment.builder().withBodyAsJson(jsonObject).build();

        payments = new ArrayList<>();
        payments.add(payment.getBodyAsJson());
    }

    @Test
    void create_payment_should_invoke_repository() {

        doReturn(Single.just(true)).when(paymentsValidations).isValid(payment);

        try {
            paymentsApi.createPayment(payment).doOnError(error -> {
            }).subscribe(paymentIdentifier -> {
            });
        } catch (Exception ex) {
        }

        verify(paymentsRepository).save(payment.getBodyAsJson());
    }


    @Test
    void getAllPayments_payment_should_invoke_repository() {

        try {
            paymentsApi.getAllPayments().doOnError(error -> {
            }).subscribe(paymentIdentifier -> {
            });
        } catch (Exception ex) {
        }

        verify(paymentsRepository).getAll();
    }


    @Test
    void create_payment_should_return_payment_identifier() {

        doReturn(Single.just(true)).when(paymentsValidations).isValid(payment);

        try {
            paymentsApi.createPayment(payment).doOnError(error -> {
            }).subscribe(paymentIdentifier -> {
                assertThat(paymentIdentifier).isEqualTo(paymentIdentifier);
            });
        } catch (Exception ex) {
        }

    }

    @Test
    void create_payment_should_throw_invalid_payment_if_id_null() {

        doReturn(Single.just(false)).when(paymentsValidations).isValid(payment);

        try {
            paymentsApi.createPayment(payment).doOnError(error -> {
                assertThat(error.getClass()).isEqualTo(com.gutmox.payments.exceptions.InvalidPayment.class);
            }).subscribe(paymentIdentifier -> {
                assertThat(paymentIdentifier).isEqualTo(paymentIdentifier);
            });
        } catch (Exception ex) {
        }

    }

    @Test
    void get_payment_should_invoke_repository() {

        doReturn(Single.just(jsonObject)).when(paymentsRepository).get(paymentIdentifier);

        paymentsApi.getPayment(paymentIdentifier);

        verify(paymentsRepository).get(paymentIdentifier);
    }


    @Test
    void get_payment_should_get_payment_object() {

        doReturn(Single.just(jsonObject)).when(paymentsRepository).get(paymentIdentifier);

        paymentsApi.getPayment(paymentIdentifier).subscribe(payment -> {

            assertThat(payment).isEqualTo(this.payment);
        });
    }

    @Test
    void getAllPayments_should_return_all_payments_from_repo() {
        doReturn(Single.just(payments)).when(paymentsRepository).getAll();

        paymentsApi.getAllPayments().subscribe(allPayments -> {

            assertThat(allPayments).isEqualTo(this.payments);
        });
    }
}