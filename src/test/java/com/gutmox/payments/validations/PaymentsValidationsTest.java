package com.gutmox.payments.validations;

import com.gutmox.payments.domain.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PaymentsValidationsTest {

    private PaymentsValidations paymentsValidations;


    @BeforeEach
    void setUp() {
        paymentsValidations = new PaymentsValidations();
    }

    @Test
    void isValid_should_be_false_without_id() {

        paymentsValidations.isValid(Payment.builder().build()).subscribe(isValid ->
            assertThat(isValid).isFalse());

    }

    @Test
    void isValid_should_be_false_with_id_empty() {

        paymentsValidations.isValid(Payment.builder().withId("").build()).subscribe(isValid ->
                assertThat(isValid).isFalse());

    }

    @Test
    void isValid_should_be_true_with_id() {

        paymentsValidations.isValid(Payment.builder().withId(UUID.randomUUID().toString()).build()).subscribe(isValid ->
                assertThat(isValid).isFalse());

    }
}