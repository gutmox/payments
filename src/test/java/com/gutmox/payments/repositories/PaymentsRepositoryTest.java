package com.gutmox.payments.repositories;

import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.mongo.MongoClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class PaymentsRepositoryTest {

    private PaymentsRepository paymentsRepository;

    @Mock
    private MongoClient mongoClient;

    private String paymentIdentifier = UUID.randomUUID().toString();

    private JsonObject payment = new JsonObject()
            .put("_id", paymentIdentifier)
            .put("payment_identifier", paymentIdentifier);

    private List<JsonObject> payments;

    @BeforeEach
    void setUp() {

        paymentsRepository = new PaymentsRepository(mongoClient);

        payments = new ArrayList<>();
        payments.add(payment);
    }

    @Test
    void save_should_save_payment() {

        doReturn(Single.just("id")).when(mongoClient).rxSave("payments", payment);

        paymentsRepository.save(payment);

        verify(mongoClient).rxSave("payments", payment);
    }

    @Test
    void save_should_return_payment_identifier() {

        doReturn(Single.just("id")).when(mongoClient).rxSave("payments", payment);

        Single<String> savedAccountNumber = paymentsRepository.save(payment);

        savedAccountNumber.subscribe(res -> {

            assertThat(res).isEqualTo(paymentIdentifier);
        });
    }

    @Test
    void get_should_find_payment() {

        doReturn(Single.just("id")).when(mongoClient)
                .rxFindOne("payments",
                        new JsonObject().put("_id", paymentIdentifier), null);

        paymentsRepository.get(paymentIdentifier);

        verify(mongoClient).rxFindOne("payments", new JsonObject().put("_id", paymentIdentifier), null);
    }

    @Test
    void get_should_find_one_payment() {

        doReturn(Single.just("id")).when(mongoClient)
                .rxFindOne("payments",
                        new JsonObject().put("_id", paymentIdentifier), null);

        paymentsRepository.get(paymentIdentifier);

        verify(mongoClient).rxFindOne("payments", new JsonObject().put("_id", paymentIdentifier), null);
    }

    @Test
    void get_should_return_payment() {

        doReturn(Single.just("id")).when(mongoClient)
                .rxFindOne("payments",
                        new JsonObject().put("_id", paymentIdentifier), null);

        paymentsRepository.get(paymentIdentifier).subscribe(payment ->{
            assertThat(payment).isEqualTo(this.payment);
        });
    }

    @Test
    void get_all_should_return_all_data_stored() {

        doReturn(Single.just(payments)).when(mongoClient)
                .rxFind("payments", new JsonObject());

        paymentsRepository.getAll().subscribe(payments ->{
            assertThat(payments).isEqualTo(this.payments);
        });
    }
}