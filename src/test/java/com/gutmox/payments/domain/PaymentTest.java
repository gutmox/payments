package com.gutmox.payments.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PaymentTest {

    private String id = UUID.randomUUID().toString();

    Payment payment1 = Payment.builder().withId(id).build();

    Payment payment2 = Payment.builder().withId(id).build();

    @Test
    void two_payments_should_be_equals() {

        assertThat(payment1).isEqualTo(payment2);
        assertThat(payment2).isEqualTo(payment1);
    }

    @Test
    void two_payments_should_be_equals_in_hashmap() {

        Map<Integer, Payment> paymentap = new HashMap<>();
        paymentap.put(1, payment1);
        paymentap.put(2, payment2);
        assertThat(paymentap.get(1)).isEqualTo(paymentap.get(2));
    }

}