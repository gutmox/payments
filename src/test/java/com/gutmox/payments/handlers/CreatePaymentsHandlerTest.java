package com.gutmox.payments.handlers;

import com.gutmox.payments.api.PaymentsApiImpl;
import com.gutmox.payments.domain.Payment;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CreatePaymentsHandlerTest {

    private CreatePaymentsHandler createPaymentsHandler;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerResponse response;

    @Mock
    private PaymentsApiImpl paymentsApi;

    private JsonObject jsonObject;

    private Payment payment;

    private String paymentIdentifier = UUID.randomUUID().toString();
    private String id = UUID.randomUUID().toString();

    @BeforeEach
    void setUp() {

        createPaymentsHandler = new CreatePaymentsHandler(paymentsApi);

        jsonObject = new JsonObject()
                .put("id", id);

        payment = Payment.builder().withBodyAsJson(jsonObject).build();
    }

    @Test
    void should_return_payment_created() {

        given();

        createPaymentsHandler.handle(context);

        verify(response).end(new JsonObject()
                .put("id", id)
                .put("payment_identifier", paymentIdentifier)
                .encode());
    }

    @Test
    void should_pass_params_to_api() {

        given();

        createPaymentsHandler.handle(context);

        verify(paymentsApi).createPayment(payment);
    }

    private void given() {

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        doReturn(jsonObject).when(context).getBodyAsJson();

        doReturn(Single.just(paymentIdentifier)).when(paymentsApi).createPayment(payment);
    }

}