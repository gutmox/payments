package com.gutmox.payments.handlers;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class StatusHandlerTest {

    private StatusHandler statusHandler;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerResponse response;

    @BeforeEach
    void setUp(){
        statusHandler = new StatusHandler();
    }

    @Test
    void status_should_return_OK() {

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        statusHandler.handle(context);

        verify(response).end(new JsonObject().put("status", "Show must go on").encode());
    }
}