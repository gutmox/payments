package com.gutmox.payments.handlers;

import com.gutmox.payments.api.PaymentsApiImpl;
import com.gutmox.payments.domain.Payment;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServerRequest;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GetPaymentHandlerTest {

    private GetPaymentHandler getPaymentHandler;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerRequest request;

    @Mock
    private HttpServerResponse response;

    @Mock
    private PaymentsApiImpl paymentsApi;

    private JsonObject jsonObject;

    private Payment payment;

    private String paymentIdentifier = UUID.randomUUID().toString();
    private String id = UUID.randomUUID().toString();

    @BeforeEach
    void setUp() {

        getPaymentHandler = new GetPaymentHandler(paymentsApi);

        jsonObject = new JsonObject()
                .put("id", id);

        payment = Payment.builder().withBodyAsJson(jsonObject).build();
    }

    @Test
    void get_payment_should_invoke_api() {

        doReturn(request).when(context).request();

        doReturn(paymentIdentifier).when(request).getParam("paymentIdentifier");

        doReturn(Single.just(payment)).when(paymentsApi).getPayment(paymentIdentifier);

        getPaymentHandler.handle(context);

        verify(paymentsApi).getPayment(paymentIdentifier);
    }

    //@Test
    void get_payment_should_return_payment() {

        doReturn(request).when(context).request();

        doReturn(paymentIdentifier).when(request).getParam("paymentIdentifier");

        doReturn(Single.just(payment)).when(paymentsApi).getPayment(paymentIdentifier);

        doReturn(response).when(context).response();

        doReturn(jsonObject).when(context).getBodyAsJson();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        getPaymentHandler.handle(context);

        verify(response).end(payment.getBodyAsJson().encode());
    }
}