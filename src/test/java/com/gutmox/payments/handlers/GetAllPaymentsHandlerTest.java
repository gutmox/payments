package com.gutmox.payments.handlers;

import com.gutmox.payments.api.PaymentsApiImpl;
import com.gutmox.payments.domain.Payment;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.http.HttpServerRequest;
import io.vertx.reactivex.core.http.HttpServerResponse;
import io.vertx.reactivex.ext.web.RoutingContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GetAllPaymentsHandlerTest {

    private GetAllPaymentsHandler getAllPaymentsHandler;

    @Mock
    private PaymentsApiImpl paymentsApi;

    @Mock
    private RoutingContext context;

    @Mock
    private HttpServerResponse response;

    @Mock
    private HttpServerRequest request;

    private List<JsonObject> payments;


    @BeforeEach
    void setUp() {

        getAllPaymentsHandler = new GetAllPaymentsHandler(paymentsApi);

        payments = new ArrayList<>();
        payments.add(Payment.builder().withBodyAsJson(new JsonObject().put("payment_identifier",
                UUID.randomUUID().toString())).build().getBodyAsJson());
    }

    @Test
    void get_all_payments_should_invoke_api() {

        doReturn(request).when(context).request();

        doReturn(Single.just(payments)).when(paymentsApi).getAllPayments();

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        getAllPaymentsHandler.handle(context);

        verify(paymentsApi).getAllPayments();
    }

    @Test
    void get_all_payments_should_return_api_list() {

        doReturn(request).when(context).request();

        doReturn(Single.just(payments)).when(paymentsApi).getAllPayments();

        doReturn(response).when(context).response();

        doReturn(response).when(response).putHeader("content-type", "application/json");

        getAllPaymentsHandler.handle(context);

        verify(response).end(payments.toString());
    }
}